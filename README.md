# ubuntu-base

Base Ubuntu LTS container to use in other projects

# Build Your Own

1. git clone https://gitlab.com/doritoes/ubuntu-base
1. cd ubuntu-base
1. docker build -t ubuntu-base:latest .

# Raspberry Pi (and other compatible ARM systems)

## 32-bit OS arm32v6
Pi 1, original Pi 2 B, Compute Module 1, Pi Zero, Pi Zero W

Ubuntu does not have an arm32v6 image anymore. Look to arm32v6/apline.

armhf organization is deprecated in favor of the more-specific arm32v7 and arm32v6 organizations. [link](https://github.com/docker-library/official-images#architectures-other-than-amd64)

## 32-bit OS arm32v7/arm32v8
Pi 2B and later including Pi Zero 2W
1. git clone https://gitlab.com/doritoes/ubuntu-base
2. cd ubuntu-base
3. docker build -t ubuntu-base:armhf -f Dockerfile.armhf .

## 64-bit OS arm64v8
Raspberry RPi 2 Model B v1.2 and later running 64-bit OS

1. git clone https://gitlab.com/doritoes/ubuntu-base
2. cd ubuntu-base
3. docker build -t ubuntu-base:arm64v8 -f Dockerfile.arm64v8 .

# Use In Another Image

 * FROM doritoes/ubuntu-base:latest
 * FROM doritoes/ubuntu-base:arm64v8
 * FROM doritoes/ubuntu-base:arm32v7
